package api

import (
	"github.com/a-h/templ"
	"github.com/dotcomdev/battle-tracker/component"
	"github.com/dotcomdev/battle-tracker/component/user"
	"github.com/labstack/echo/v4"
	"net/http"
)

func StartApi() {
	e := echo.New()

	e.Static("/", "public")

	e.File("/", "public/index.html")

	e.GET("/test", func(c echo.Context) error { return render(c, 200, component.Hello("hello")) })

	e.GET("/click-sign-in", func(c echo.Context) error { return render(c, 200, user.SignInForm()) })
	e.GET("/click-sign-up", func(c echo.Context) error { return render(c, 200, user.SignUpForm()) })

	e.Logger.Fatal(e.Start(":1323"))
}

func render(ctx echo.Context, status int, t templ.Component) error {
	ctx.Response().Writer.WriteHeader(status)

	err := t.Render(ctx.Request().Context(), ctx.Response().Writer)
	if err != nil {
		return ctx.String(http.StatusInternalServerError, "failed to render response template")
	}

	return nil
}
