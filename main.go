package main

import "github.com/dotcomdev/battle-tracker/api"

func main() {
	api.StartApi()
}
